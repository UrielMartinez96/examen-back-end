﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('carreraCtrl', ['$scope', 'CarreraService', function ($scope, CarreraService) {
          //  $scope.liebre = null;
          //  $scope.tortuga = null;
          //  $scope.carrera = null;
            getLiebre();
            getTortuga();
            //getResultados();

            function getLiebre() {
                CarreraService.getLiebre().then(function (result) {
                    $scope.liebre = result;
                });
            }

            function getTortuga() {
                CarreraService.getTortuga().then(function (result) {
                    $scope.tortuga = result;
                });
            }

            $scope.editLiebre = function editLiebre(liebre) {
                CarreraService.editLiebre(liebre).then(function () {
                    console.log("exito");
                }, function () {
                        console.log("fracaso");
                });
            };

            $scope.editTortuga = function editTortuga(tortuga) {
                CarreraService.editTortuga(tortuga).then(function () {
                    console.log("exito");
                }, function () {
                    console.log("fracaso");
                });
            };

            $scope.simularCarrera = function simularCarrera() {
                console.log("me has tocado");
                CarreraService.simularCarrera().then(function () {
                    getResultados();
                    console.log("exito");
                }, function () {
                    console.log("fracaso");
                });
            };

            function getResultados() {
                CarreraService.getResultados().then(function (result) {
                    $scope.carrera = result;
                });
            }
        }]);
})();

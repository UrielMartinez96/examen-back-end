﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('CarreraService', ['$http', '$q', function ($http, $q) {
            var service = {};

            service.getLiebre = function () {
                var deferred = $q.defer();
                $http.get('/Carrera/GetLiebre').then(function (result) {
                    deferred.resolve(result.data);
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };

            service.getTortuga = function () {
                var deferred = $q.defer();
                $http.get('/Carrera/GetTortuga').then(function (result) {
                    deferred.resolve(result.data);
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };

            service.editLiebre = function (liebre) {
                var deferred = $q.defer();
                $http.post('/Carrera/EditLiebre', liebre).then(function () {
                    deferred.resolve();
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };

            service.editTortuga = function (tortuga) {
                console.log(tortuga);
                var deferred = $q.defer();
                $http.post('/Carrera/EditTortuga', tortuga).then(function () {
                    deferred.resolve();
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };

            service.simularCarrera = function () {
                var deferred = $q.defer();
                $http.get('/Carrera/SimularCarrera').then(function () {
                    deferred.resolve();
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };

            service.getResultados = function () {
                var deferred = $q.defer();
                $http.get('/Carrera/ResultadosCarrera').then(function (result) {
                    deferred.resolve(result.data);
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };



            return service;

        }]);
})();
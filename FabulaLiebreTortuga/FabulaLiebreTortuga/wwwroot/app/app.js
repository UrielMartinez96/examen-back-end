﻿(function () {
    'use strict';

    angular
        .module('app', [
            'ngRoute'
        ])
        .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
            $locationProvider.hashPrefix('');
            $routeProvider
                .when('/', {
                    templateUrl: '/app/templates/carrera.html',
                    controller: 'carreraCtrl'
                    
                })
                .otherwise({ redirectTo: '/' });
        }]);

})();
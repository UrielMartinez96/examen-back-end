﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FabulaLiebreTortuga.Models
{
    public abstract class Animal
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public int velocidad { get; set; }

        public Animal(int velocidad)
        {
            this.velocidad = velocidad;
        }


        public Animal()
        {

        }

    }
}

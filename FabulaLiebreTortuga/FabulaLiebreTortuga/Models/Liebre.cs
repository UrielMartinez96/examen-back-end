﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FabulaLiebreTortuga.Models
{
    public class Liebre : Animal
    {
        
        public List<string>? antivalores { get; set; }


        public Liebre(int velocidad) : base(velocidad)
        {
            this.velocidad = velocidad;
            this.antivalores = null;
        }

        public Liebre()
        {
            this.antivalores = null;
        }


        ~Liebre()
        {
            Console.WriteLine("Hasta aquí llegamos");
        }

    }
}

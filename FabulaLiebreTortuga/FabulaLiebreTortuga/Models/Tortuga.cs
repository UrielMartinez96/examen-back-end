﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FabulaLiebreTortuga.Models
{
    public class Tortuga : Animal
    {
        public List<string>? valores { get; set; }

        public Tortuga(int velocidad) : base(velocidad)
        {
            this.velocidad = velocidad;
            this.valores = null;
        }
        public Tortuga()
        {
            this.valores = null;
        }

        ~Tortuga()
        {
            Console.WriteLine("Hasta aquí llegamos");
        }


    }
}

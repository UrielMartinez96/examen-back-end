﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FabulaLiebreTortuga.Models;

namespace FabulaLiebreTortuga.Controllers
{
    public class CarreraController : Controller
    {

        public Tortuga tortuga = null;
        public Liebre liebre = null;
        public int distanciaActualLiebre;
        public int distanciaActualTortuga;
        public int recorridoTortuga;
        public int recorridoLiebre;
        public CarreraController()
        {
            distanciaActualLiebre = 0;
            distanciaActualTortuga = 0;
            recorridoTortuga = 0;
            recorridoLiebre = 0;
            liebre = new Liebre(20);
            tortuga = new Tortuga(13);
        }

        //Clase prueba para trabajar con JSON 
        public JsonResult GetLiebre()
        {

            return Json(liebre);
        }

        public JsonResult GetTortuga()
        {
            return Json(tortuga);
        }

        [HttpPost]
        public JsonResult EditLiebre(Liebre liebre)
        {
            this.liebre = liebre;
            return Json(null);
        }

        [HttpPost]
        public JsonResult EditTortuga(Tortuga tortu)
        {
            Console.WriteLine(tortu.nombre);
            this.tortuga = tortu;
            return Json(null);
        }


        public JsonResult ResultadosCarrera()
        {
            Console.WriteLine(recorridoTortuga);
            return Json(this.recorridoTortuga);
        }

        public async Task SimularCarrera()
        {           
            var distancia = 100;

            do
            {
                Console.WriteLine("tortuga: "+distanciaActualTortuga);
                Console.WriteLine("liebre: "+distanciaActualLiebre);
                this.distanciaActualTortuga = await AvanzarTortuga(distanciaActualTortuga);
                if (this.distanciaActualTortuga > distanciaActualLiebre)
                {
                    if (distanciaActualLiebre+liebre.velocidad < distanciaActualTortuga)
                    {
                        this.distanciaActualLiebre = await AvanzarLiebre(distanciaActualLiebre);
                    }
                }
            } while (this.distanciaActualTortuga < distancia || this.distanciaActualLiebre < distancia) ;
        }


        public async Task<int> AvanzarLiebre(int distanciaActual)
        {
            var Task = new Task<int>(()=> distanciaActual += liebre.velocidad );
            Task.Start();
            int result = await Task;
            return result;
        }

        public async Task<int> AvanzarTortuga(int distanciaActual)
        {
            var Task = new Task<int>(() => distanciaActual += tortuga.velocidad);
            Task.Start();
            int result = await Task;
            return result;
        }

        ~CarreraController()
        {
            Console.WriteLine("La carrera ha terminado");
        }


    }
}